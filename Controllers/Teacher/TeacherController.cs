﻿using Rotativa;
using SyllabusAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;

namespace SyllabusAutomation.Controllers.Teacher
{
    public class TeacherController : Controller
    {
        SyllabusAutomationEntities db = new SyllabusAutomationEntities();
        // GET: Teacher
        #region Selection
        public ActionResult Selection()
        {
            int uid = (int)Session["uid"];
            var user = db.Users.Find(uid);
            var programs = db.Programs.Where(x => x.DepartmentId == user.DepartmentId).OrderBy(x => x.ShortName).ToList();
            ViewBag.ProgramList = new SelectList(programs, "ProgramId", "ShortName");
            var sessions = db.Sessions.Where(x => x.DepartmentId == user.DepartmentId).ToList();
            ViewBag.SessionList = new SelectList(sessions, "SessionId", "SessionName");
            var years = db.EduYears.ToList();
            ViewBag.YearList = new SelectList(years, "YearId", "YearName");
            var semesters = db.Semesters.ToList();
            ViewBag.semesterList = new SelectList(semesters, "SemesterId", "SemesterName");
            return View();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Selection(FormCollection frm)
        {
            int uid = (int)Session["uid"];
            var user = db.Users.Find(uid);
            Session["programId"] = frm["ProgramId"];
            Session["sessionId"] = frm["SessionId"];
            Session["year"] = frm["YearId"];
            Session["semester"] = frm["SemisterId"];

            return RedirectToAction("CourseList", "Teacher");
        }

        #endregion

        #region CourseList
        public ActionResult CourseList()
        {
            int uid = (int)Session["uid"];
            var user = db.Users.Find(uid);
            var pid = Convert.ToInt32(Session["programId"]);
            var sessionId = Convert.ToInt32(Session["sessionId"]);
            var yearId = Convert.ToInt32(Session["year"]);
            var semesterId = Convert.ToInt32(Session["semester"]);


            ViewBag.program = db.Programs.Find(pid).ProgramName;
            ViewBag.session = db.Sessions.Find(sessionId).SessionName;
            ViewBag.year = db.EduYears.Find(yearId).YearName;
            ViewBag.semester = db.Semesters.Find(semesterId).SemesterName;
            
            ViewBag.CourseTypeList = new SelectList(db.CourseTypes.Where(x => x.ProgramId == pid && x.DepartmentId == user.DepartmentId).ToList(), "CourseTypeId", "CourseType1");
            var plos = db.Courses.Where(x => x.ProgramId == pid && x.SessionId == sessionId
            && x.YearId == yearId && x.SemisterId == semesterId && x.DepartmentId == user.DepartmentId && x.UserId == user.UserID).ToList();
            var tuple = new Tuple<Course, List<Course>>(new Course(), plos);
            return View(tuple);
        }

        [HttpGet]
        public ActionResult UpdateCourseList(int id)
        {
            int uid = (int)Session["uid"];
            var user = db.Users.Find(uid);
            var pid = Convert.ToInt32(Session["programId"]);
            var sessionId = Convert.ToInt32(Session["sessionId"]);
            var yearId = Convert.ToInt32(Session["year"]);
            var semesterId = Convert.ToInt32(Session["semester"]);


            var mission = db.Courses.Find(id);

            if (mission == null)
            {
                return HttpNotFound();
            }

            ViewBag.program = db.Programs.Find(pid).ProgramName;
            ViewBag.session = db.Sessions.Find(sessionId).SessionName;
            ViewBag.year = db.EduYears.Find(yearId).YearName;
            ViewBag.semester = db.Semesters.Find(semesterId).SemesterName;
            ViewBag.CourseTypeList = new SelectList(db.CourseTypes.Where(x => x.ProgramId == pid && x.DepartmentId == mission.DepartmentId).ToList(), "CourseTypeId", "CourseType1", mission.CourseTypeId);
            var missions = db.Courses.Where(x => x.ProgramId == pid && x.SessionId == sessionId
            && x.YearId == yearId && x.SemisterId == semesterId && x.DepartmentId == user.DepartmentId && x.UserId == user.UserID).ToList();


            var tuple = new Tuple<Course, List<Course>>(mission, missions);
            ViewBag.data = true;
            return View("CourseList", tuple);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateCourseList(int id, FormCollection form)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var peo = db.Courses.Find(id);
                    if (peo == null)
                    {
                        return HttpNotFound();
                    }
                    peo.CourseTypeId = Convert.ToInt32(form["Item1.CourseTypeId"]);
                    var mark = db.Marks.Find(peo.CourseTypeId);
                    peo.CourseCode = form["Item1.CourseCode"];
                    peo.CourseTitle = form["Item1.CourseTitle"];
                    peo.Credit = form["Item1.Credit"].AsFloat();
                    peo.CourseSummary = form["Item1.CourseSummary"];
                    peo.MarksId = mark.MarksId;
                    
                    db.SaveChanges();
                    TempData["msg"] = "Course Updated Successfully!";
                }
            }
            catch
            {
                TempData["msg"] = "Something Error Occurred! Try Again... ";
            }

            return RedirectToAction("CourseList", "Teacher");
        }


        #endregion

        #region CourseWise Syllabus
        public ActionResult ProgramSelection()
        {
            int uid = (int)Session["uid"];
            var user = db.Users.Find(uid);
            var programs = db.Programs.Where(x => x.DepartmentId == user.DepartmentId).OrderBy(x => x.ShortName).ToList();
            ViewBag.ProgramList = new SelectList(programs, "ProgramId", "ShortName");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProgramSelection(FormCollection frm)
        {
            int uid = (int)Session["uid"];
            var user = db.Users.Find(uid);
            var programs = db.Programs.Where(x => x.DepartmentId == user.DepartmentId).OrderBy(x => x.ShortName).ToList();
            ViewBag.ProgramList = new SelectList(programs, "ProgramId", "ShortName");
            var programId = Convert.ToInt32(frm["ProgramId"]);
            return RedirectToAction("MyCourses", new RouteValueDictionary(new { Controller = "Teacher", Action = "MyCourses", id = programId }));
            
        }
        public ActionResult MyCourses(int? id)
        {
            int uid = (int)Session["uid"];
            var user = db.Users.Find(uid);
            var pid = id;
            ViewBag.program = db.Programs.Find(pid).ProgramName;
            var courses = db.Courses.Where(x => x.ProgramId == pid && x.DepartmentId == user.DepartmentId && x.UserId == user.UserID).ToList();
            return View(courses);
        }
        #endregion


        #region PDF Creation

        public ActionResult YearSelection()
        {
            int uid = (int)Session["uid"];
            var user = db.Users.Find(uid);
            var programs = db.Programs.Where(x => x.DepartmentId == user.DepartmentId).OrderBy(x => x.ShortName).ToList();
            ViewBag.ProgramList = new SelectList(programs, "ProgramId", "ShortName");
            var sessions = db.Sessions.Where(x => x.DepartmentId == user.DepartmentId && x.IsActive==true).OrderBy(x => x.SessionId).ToList();
            ViewBag.SessionList = new SelectList(sessions, "SessionId", "SessionName");
            var years = db.EduYears.ToList();
            ViewBag.YearList = new SelectList(years, "YearId", "YearName");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult YearSelection(FormCollection frm)
        {
            //int userid = (int)Session["uid"];
            //var user = db.Users.Find(userid);
            Session["progrmId"] = frm["ProgramId"];
            Session["sessionId"] = frm["SessionId"];
            Session["yearId"] = frm["YearId"];
            return RedirectToAction("SubjectList", new RouteValueDictionary(new { Controller = "Teacher", Action = "SubjectList", pid = Convert.ToInt32(Session["progrmId"]), sid = Convert.ToInt32(Session["sessionId"]), yid = Convert.ToInt32(Session["yearId"]) }));
        }

        public ActionResult SubjectList(int? pid, int? sid, int? yid)
        {
            //int uid = (int)Session["uid"];
            //var user = db.Users.Find(uid);
            //var prgmId = Convert.ToInt32(Session["progrmId"]);
            ViewBag.Program = db.Programs.Find(pid).ShortName;
            ViewBag.Session = db.Sessions.Find(sid).SessionName;
            ViewBag.Year = db.EduYears.Find(yid).YearName;
            var q = db.Courses.Where(x => x.ProgramId == pid && x.SessionId == sid && x.YearId == yid).OrderBy(x=>x.SemisterId).ToList();
            return View(q);
        }
        public ActionResult GenerateSyllabus(int ? id)
        {
            Session["courseId"] = id;
            ViewBag.CourseDetail = db.Courses.Where(x=>x.CourseId == id).ToList();
            return View();
        }

        //public ActionResult PrintSyllabus(int id)
        //{
        //    return new ActionAsPdf(
        //                   "PrintSyllabus",
        //                   new { id = id })
        //    { FileName = "Syllabus.pdf" };
        //}
        public ActionResult PrintSyllabus()
        {
            var courseId = Convert.ToInt32(Session["courseId"]);
            return new Rotativa.ActionAsPdf("GenerateSyllabus", new RouteValueDictionary(new { Controller = "Teacher", Action = "GenerateSyllabus", id = courseId }));

        }
        public ActionResult FullSyllabus(int? pid, int? sid, int? uid)
        {
            if (Session["uid"] == null)
            {
                Session["uid"] = uid;
                Session["progrmId"] = pid;
                Session["sessionId"] = sid;
            }

            //int usrid = (int)Session["uid"];
            ViewBag.uid = uid;
            var user = db.Users.Find(uid);


            //var course = db.Courses.Find(id);
            //var prgmId = (int)course.ProgramId;


            var program = db.Programs.Where(x => x.ProgramId == pid).FirstOrDefault();
            var programdetail = db.Programs.Where(x => x.ProgramId == pid).ToList();
            ViewBag.ProgramFullName = program.ProgramName;
            ViewBag.ProgramShortName = program.ShortName;
            ViewBag.Program = programdetail;
            ViewBag.CurriculumStructureList = db.CurriculmnStructures.Where(x => x.ProgramId == pid && x.DepartmentId == user.DepartmentId).ToList();

            var session = db.Sessions.Where(x => x.SessionId == sid).FirstOrDefault();
            ViewBag.SessionName = session.SessionName;

            ViewBag.userDetail = db.Users.Where(x => x.UserID == uid).ToList();
            ViewBag.missionofUniversity = db.MissionOfUniversities.Where(x => x.UniversityId == user.UniversityId).ToList();
            ViewBag.deptDetail = db.Departments.Where(x => x.DepartmentId == user.DepartmentId).ToList();
            ViewBag.missionofDept = db.MissionOfDepartments.Where(x => x.DepartmentId == user.DepartmentId).ToList();
            ViewBag.peoList = db.PEOs.Where(x => x.ProgramId == pid).ToList();
            ViewBag.ploList = db.PLOes.Where(x => x.ProgramId == pid).ToList();
            ViewBag.genericSkillList = db.GenericSkills.Where(x => x.ProgramId == pid).ToList();


            /*TempData["msg"] = db.Courses.Where(x => x.CourseId == ).ToList();*/
            //var viewModelList = new List<CourseViewModel>();

            //var courses = db.Syllabus.Where(x=>x.SessionId==sid && x.ProgramId == pid && x.DeptId==user.DepartmentId).ToList();
            //foreach(var course in courses)
            //{
            //    ViewBag.courseDetail = db.Courses.Where(x => x.CourseId == course.CourseId).ToList();
            //    var q = db.LearningPlans.Where(x => x.CourseId == course.CourseId).ToList();
            //    var marks = db.Marks.Find(course.CourseId);
            //    var viewModel = new CourseViewModel
            //    {
            //        YearId =course.EduYear.YearId,
            //        SemesterId = course.Semester.SemesterId,
            //        CourseId = course.Course.CourseId,
            //        CourseCode = course.CourseCode,
            //        MarksId = marks.MarksId

            //    };
            //    viewModelList.Add(viewModel);
            //}
            //ViewBag.ViewModelList = viewModelList;

            //var r = db.Courses.Where(x => x.CourseId == id).ToList();
            //ViewBag.courseDetail = db.Courses.Where(x => x.CourseId == id).ToList();
            //ViewBag.cloDetail = db.CLOes.Where(x => x.CourseId == id).ToList();
            //ViewBag.objective = db.CourseObjectives.Where(x => x.CourseId == id).ToList();
            //    ViewBag.lesonPlans = db.LessonPlans.Where(x => x.CourseId == id).ToList();
            //ViewBag.assessments = db.AssessmentStrategies.ToList();
            //ViewBag.textBook = db.Resources.Where(x => x.ResourceType.ResourceTypeId == 1 && x.CourseId == id).ToList();
            //ViewBag.refBook = db.Resources.Where(x => x.ResourceType.ResourceTypeId == 2 && x.CourseId == id).ToList();

            //var p = db.LearningPlans.Find(id);
            //var ci = db.LearningPlans.Where(x => x.CourseId == id).ToList();
            //ViewBag.ci = ci;
            //ViewBag.cieList = db.CIEs.Where(x => x.CourseId == id && x.ProgramId == prgmId).ToList();
            //ViewBag.seeList = db.SEEs.Where(x => x.CourseId == id && x.ProgramId == prgmId).ToList();
            //ViewBag.markList = db.Marks.Where(x => x.MarksId == course.MarksId && x.ProgramId == prgmId).ToList();


            var syllabusData = db.Syllabus.Where(x => x.SessionId == sid && x.ProgramId == pid && x.DeptId == user.DepartmentId).ToList();
            var organizedData = OrganizeData(syllabusData);
            ViewBag.SyllabusData = organizedData;
            return View();
        }
        private List<List<Syllabu>> OrganizeData(List<Syllabu> syllabusData)
        {
            // Organize syllabus data by year and semester
            var organizedData = syllabusData
                .GroupBy(s => new { s.YearId, s.SemesterId })
                .OrderBy(g => g.Key.YearId)
                .ThenBy(g => g.Key.SemesterId)
                .Select(g => g.ToList())
                .ToList();

            return organizedData;
        }


        public ActionResult FullSyllabusPdf()
        {
            var userId = Convert.ToInt32(Session["uid"]);
            var programId = Convert.ToInt32(Session["progrmId"]);
            var sessionId = Convert.ToInt32(Session["sessionId"]);
            return new Rotativa.ActionAsPdf("FullSyllabus", new RouteValueDictionary(new { Controller = "GenerateSyllabus", Action = "FullSyllabus", pid = programId, sid = sessionId, uid = userId }));

        }
        #endregion
    }
}